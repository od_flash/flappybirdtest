#pragma once

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>

#include <vector>

class ResourceManager
{
public:
	static bool loadBackground(sf::Texture *texture);
	static bool loadGround(sf::Texture* texture);
	static bool loadBird(sf::Texture* texture);
	static bool loadPipe(sf::Texture* texture);
	static bool loadFontScore(sf::Font* font);
private:
	static bool loadResourceFont(sf::Font* font, int idb, const wchar_t* s);
	static bool loadResource(sf::Texture* texture, int idb, const wchar_t* s);

	static std::vector<uint8_t> s_fontBuffer;
};

