// ConsoleApplication3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "ResourceManager.h"

#include "Bird.h"
#include "Ground.h"
#include "Pipes.h"
#include "Score.h"
#include "MainMenu.h"

#include "Settings.h"

#include <iostream>
#include <SFML/Graphics.hpp>

#include <box2d/b2_world.h>
#include <box2d/b2_body.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_collision.h>
#include <box2d/b2_fixture.h>
#include <box2d/b2_world_callbacks.h>
#include <box2d/b2_contact.h>


enum class EGameState : int
{
	MAIN_MENU = 0
	, STARTED
	, END
};

static auto contactResult = EContactResult::NONE;
static auto gameState = EGameState::MAIN_MENU;

class ContactLitener : public b2ContactListener
{
public:

	void BeginContact(b2Contact* contact) override
	{
		std::cout << "Collision" << std::endl;
		auto* bA = contact->GetFixtureA()->GetBody();
		auto* bB = contact->GetFixtureB()->GetBody();

		auto tA = bA->GetTransform();
		auto tB = bB->GetTransform();

		auto* udA = static_cast<IBase*>(bA->GetUserData());
		contactResult = udA->contactResult(contact);

		if (contactResult != EContactResult::NONE)
		{
			return;
		}

		auto* udB = static_cast<IBase*>(bB->GetUserData());
		contactResult = udB->contactResult(contact);
	}
};

int main()
{
	Settings settings;

	sf::Texture txBackground;
	bool res = ResourceManager::loadBackground(&txBackground);

	if (!res)
	{
		return -1;
	}
	auto szBackground = txBackground.getSize();

	txBackground.setRepeated(false);
	sf::IntRect ir(0, 0, szBackground.x, szBackground.y);
	sf::Sprite sBackground(txBackground, ir);

	b2Vec2 gravity(0.0f, settings.gravity);
	b2World world(gravity);

	ContactLitener cl;
	world.SetContactListener(static_cast<b2ContactListener*>(&cl));

	sf::RenderWindow window(sf::VideoMode(szBackground.x, szBackground.y), "SFML works!");

	Bird bird(world, window, settings);
	Ground ground(world, window, settings);
	Pipes pipes(world, window, settings);
	Score score(world, window, settings);
	MainMenu mainMenu(world, window, settings);

	auto szWindow = window.getSize();

	window.setFramerateLimit(30);

	sf::Clock clock;
	sf::Int64 lastTimeMcSec = clock.getElapsedTime().asMicroseconds();

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::EventType::KeyPressed:
				if (event.key.code == sf::Keyboard::Escape)
				{
					window.close();
					continue;
				}
			case sf::Event::EventType::MouseButtonPressed:
				if (gameState == EGameState::MAIN_MENU)
				{
					bird.restart();
					ground.restart();
					pipes.restart();
					score.restart();
					mainMenu.stop();
					gameState = EGameState::STARTED;
				}
				else if (gameState == EGameState::STARTED)
				{
					bird.jumpUp();
					pipes.jumpUp();
				}
				else if (gameState == EGameState::END)
				{
					bird.restart();
					ground.restart();
					pipes.restart();
					score.restart();
					mainMenu.restart();
					gameState = EGameState::MAIN_MENU;
				}
				break;

			default:
				break;
			}
			if (event.type == sf::Event::Closed)
				window.close();
		}

		if (!window.isOpen())
		{
			break;
		}

		auto eTime = clock.getElapsedTime();

		int32 velocityIterations = 100;
		int32 positionIterations = 100;

		world.Step(float(eTime.asMicroseconds() - lastTimeMcSec) / 1000.f / 1000.f, velocityIterations, positionIterations);

		switch (gameState)
		{
		case EGameState::MAIN_MENU:

			break;
		case EGameState::STARTED:
			if (contactResult == EContactResult::LOSE)
			{
				ground.stop();
				pipes.stop();
				score.stop();
				bird.stop();
				gameState = EGameState::END;
			}
			else if (contactResult == EContactResult::SCORE)
			{
				score.inc();
			}
			break;
		case EGameState::END:
			break;
		default:
			break;
		}

		lastTimeMcSec = eTime.asMicroseconds();

		//window.clear();

		sBackground.setPosition(0.f, 0.f);
		window.draw(sBackground);

		pipes.draw(eTime);
		ground.draw(eTime);
		bird.draw(eTime);
		score.draw(eTime);
		mainMenu.draw(eTime);

		window.display();

		contactResult = EContactResult::NONE;
	}

	return 0;
}