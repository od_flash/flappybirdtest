#pragma once

#include "IBase.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_math.h>

class b2World;
class b2Body;
class b2Fixture;
struct b2Vec2;

class Settings;

enum class EScoreSate : int
{
	DRAW_SCORE = 0
	, DRAW_SCORE_TABEL
};

class Score : protected IBase
{
public:
	Score(b2World& b2World, sf::RenderWindow& window, Settings& s);
	bool draw(const sf::Time& ts) override final;
	bool restart() override final;
	virtual bool stop() override final;

	bool save() { return true; }
	bool load() { return true; }

	void set(int score);
	void inc();
	void dec();

private:
	
	int m_scoreCarrent;
	int m_scoreHigh;

	sf::Font m_font;
	sf::Text m_text;

	EScoreSate m_scoreState;
	sf::RectangleShape m_rectHsTable;
	sf::Font m_fontHs;
	sf::Text m_textHsText;
	sf::Text m_textHsLabelScore;
	sf::Text m_textHsLabelHighScore;

};

