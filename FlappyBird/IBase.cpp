#include "IBase.h"
#include "Settings.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <box2d/b2_math.h>

IBase::IBase(b2World& b2World, sf::RenderWindow& window, Settings& s)
	: m_b2World(b2World)
	, m_settings(s)
	, m_window(window)
	, e1(r())
{

}

bool IBase::stop() 
{ 
	return true; 
}

EContactResult IBase::contactResult(const b2Contact*)
{ 
	return EContactResult::NONE; 
}

b2Vec2 IBase::toWorld(const sf::Vector2i& sz) const
{
	b2Vec2 bv;
	bv.x = float(sz.x) / m_settings.SCALE;
	bv.y = float(sz.y) / m_settings.SCALE;
	return bv;
}

b2Vec2 IBase::toWorld(const sf::Vector2f& sz) const
{
	b2Vec2 bv;
	bv.x = sz.x / m_settings.SCALE;
	bv.y = sz.y / m_settings.SCALE;
	return bv;
}

b2Vec2 IBase::toWorld(const sf::Vector2u& sz) const
{
	b2Vec2 bv;
	bv.x = float(sz.x) / m_settings.SCALE;
	bv.y = float(sz.y) / m_settings.SCALE;
	return bv;
}

sf::Vector2i IBase::fromWorld(const b2Vec2& sz) const
{
	sf::Vector2i sv;
	sv.x = int(sz.x * m_settings.SCALE);
	sv.y = int(sz.y * m_settings.SCALE);
	return sv;
}
