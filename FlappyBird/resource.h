//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FlappyBird.rc
//
#define IDB_PNG1                        101
#define IDB_PNG_BACKGROUND              101
#define IDB_PNG2                        102
#define IDB_PNG_GROUND                  102
#define IDB_PNG3                        103
#define IDB_PNG_BIRD                    103
#define IDB_PNG4                        104
#define IDB_PNG_PIPE                    104
#define IDR_FD1                         113
#define IDR_FD2                         114

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        115
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
