#include "MainMenu.h"

#include "ResourceManager.h"
#include "Settings.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_world.h>
#include <box2d/b2_body.h>
#include <box2d/b2_math.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_fixture.h>

#include <string>

static const auto* txt =
L"  Press Key: start\n"
"Mouse Click: start\n"
"         Esc: exit\n"
"           R: restart";

MainMenu::MainMenu(b2World& b2World, sf::RenderWindow& window, Settings& s)
	: IBase(b2World, window, s)
	, m_text(txt, m_font)
	, m_isDrawMainMenu(true)
{
	bool res = ResourceManager::loadFontScore(&m_font);

	if (!res)
	{
		//@todo throw
		return;
	}

	auto wsz = m_window.getView().getSize();
	auto fs = float(wsz.y * float(m_settings.FONT_SIZE_PERCENT_WINDOW_HEIGHT) / 200.f);

	m_text.setCharacterSize(unsigned int(fs));
	m_text.setLineSpacing(1.3f);

	m_text.setFillColor(sf::Color::White);

	m_text.setStyle(sf::Text::Bold);
	m_text.setOutlineColor(sf::Color::Black);
	m_text.setOutlineThickness(fs / 8);
	auto bnds = m_text.getLocalBounds();
	m_text.setOrigin(bnds.width / 2, bnds.height / 2);
	m_text.setPosition({ wsz.x / 2.f, wsz.y / 2.f });

	auto hsH = bnds.height * 1.1f;
	auto hsW = bnds.width * 1.1f;

	m_rectHsTable.setSize({ hsW, hsH });
	m_rectHsTable.setOrigin(hsW / 2, hsH / 2);
	m_rectHsTable.setPosition(wsz.x / 2.f, wsz.y / 2.f);

	m_rectHsTable.setFillColor(sf::Color(0xde, 0xd8, 0xd5));
	m_rectHsTable.setOutlineColor(sf::Color(0x54, 0x38, 0x47));
	m_rectHsTable.setOutlineThickness(fs / 8);

	restart();
}

bool MainMenu::draw(const sf::Time& ts)
{
	if (m_isDrawMainMenu)
	{
		m_window.draw(m_rectHsTable);
		m_window.draw(m_text);
	}
	return true;
}

bool MainMenu::restart() 
{ 
	m_isDrawMainMenu = true; 
	return true;
}

bool MainMenu::stop()
{
	m_isDrawMainMenu = false;
	return true;
}
