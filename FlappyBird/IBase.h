#pragma once

#include <SFML/System/Vector2.hpp>

#include <random>

class b2World;
class b2Body;
class b2Contact;
class Settings;
struct b2Vec2;

namespace sf
{
	class RenderWindow;
	class Time;
}

enum class EContactResult : int
{
	NONE = 0
	, LOSE
	, SCORE
};

enum class EBodyType : int
{
	NONE = 0
	, BIRD
	, PIPE
	, GROUND
	, SKY
	, SCORE
	, SENSOR
};

class IBase
{
public:
	IBase(b2World& b2World, sf::RenderWindow& window, Settings& s);

	virtual bool draw(const sf::Time& ts) = 0;
	virtual bool restart() = 0;
	virtual bool stop();
	virtual EContactResult contactResult(const b2Contact* contact);

	virtual ~IBase() = default;

	virtual EBodyType getBodyType(const b2Body*) const { return EBodyType::NONE; }

protected:
	b2Vec2 toWorld(const sf::Vector2f& sz) const;
	b2Vec2 toWorld(const sf::Vector2i& sz) const;
	b2Vec2 toWorld(const sf::Vector2u& sz) const;
	sf::Vector2i fromWorld(const b2Vec2& sz) const;

	Settings& m_settings;

	sf::RenderWindow& m_window;
	b2World& m_b2World;

	std::random_device r;
	std::default_random_engine e1;
};

