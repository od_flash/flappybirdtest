#pragma once

#include "IBase.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_math.h>

class b2World;
class b2Body;
class b2Fixture;

class Settings;

class Bird : protected IBase
{
public:

	Bird(b2World& b2World, sf::RenderWindow& window, Settings& s);
	bool draw(const sf::Time& ts) override final;
	bool restart() override final;
	virtual bool stop() override final;

	void jumpUp();

	virtual EContactResult contactResult(const b2Contact* contact) override final;


private:
	bool init();
	bool recreateBodyBird();

	sf::Texture m_textreBird;
	sf::Time m_lastTime;

	b2Fixture* m_fixture;

	b2Body* m_body;

	std::vector<sf::Sprite> m_sprites;

	b2Vec2 m_b2StartPosition;

	bool m_isStarted;
	bool m_isDied;
};

