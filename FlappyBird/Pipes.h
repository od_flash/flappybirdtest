#pragma once

#include "IBase.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_math.h>

class b2World;
class b2Body;
class b2Fixture;
struct b2Vec2;

class Settings;


struct PipePair
{
	PipePair(b2Body* pUp, b2Body* pDown, b2Body* pSensor, float shY)
		: pipeUp(pUp)
		, pipeDown(pDown)
		, pipeSensor(pSensor)
		, shiftY(shY)
	{}

	b2Body* pipeUp = nullptr;
	b2Body* pipeDown = nullptr;
	b2Body* pipeSensor = nullptr;
	float shiftY = 0.f;
};

class Pipes : protected IBase
{
public:
	Pipes(b2World& b2World, sf::RenderWindow& window, Settings& s);
	bool draw(const sf::Time& ts) override final;
	bool restart() override final;
	bool stop() override final;

	EBodyType getBodyType(const b2Body* body) const final override;

	void jumpUp();

private:

	void recreatePipesVector();
	b2Body* createPipeBody(const b2Vec2& pos) const;
	b2Body* createPipeBodySensor(const b2Vec2& pos) const;
	sf::Texture m_textrePipe;
	std::vector<PipePair> m_pipes;

	sf::Sprite spriteUp;
	sf::Sprite spriteDown;

	bool m_isStarted;
};

