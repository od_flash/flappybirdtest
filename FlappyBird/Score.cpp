#include "Score.h"

#include "ResourceManager.h"
#include "Settings.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_world.h>
#include <box2d/b2_body.h>
#include <box2d/b2_math.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_fixture.h>

#include <string>

Score::Score(b2World& b2World, sf::RenderWindow& window, Settings& s)
	: IBase(b2World, window, s)
	, m_scoreCarrent(0)
	, m_scoreHigh(0)
	, m_scoreState(EScoreSate::DRAW_SCORE)
	, m_textHsLabelScore(L"SCORE", m_fontHs)
	, m_textHsLabelHighScore(L"HIGH SCORE", m_fontHs)
{
	bool res = ResourceManager::loadFontScore(&m_font);
	res = ResourceManager::loadFontScore(&m_fontHs);

	if (!res)
	{
		//@todo throw
		return;
	}

	auto wsz = m_window.getView().getSize();
	auto fs = float(wsz.y * float(m_settings.FONT_SIZE_PERCENT_WINDOW_HEIGHT) / 100.f);

	{
		m_text.setFont(m_font);
		m_text.setCharacterSize(unsigned int(fs));

		m_text.setFillColor(sf::Color::White);

		m_text.setStyle(sf::Text::Bold);
		m_text.setOutlineColor(sf::Color::Black);
		m_text.setOutlineThickness(fs / 8);
		m_text.setPosition({ fs / 2.f, fs / 2.f });
	}


	auto hsH = float(wsz.y * float(m_settings.TABLE_HIGHSCORE_PERCENT_WINDOW_HEIGHT) / 100.f);
	auto hsW = float(wsz.y * float(m_settings.TABLE_HIGHSCORE_PERCENT_WINDOW_HEIGHT) / 150.f);

	m_rectHsTable.setSize({ hsW, hsH });
	m_rectHsTable.setOrigin(hsW / 2, hsH / 2);
	m_rectHsTable.setPosition(wsz.x / 2.f, wsz.y / 2.f);

	m_rectHsTable.setFillColor(sf::Color(0xde, 0xd8, 0xd5));
	m_rectHsTable.setOutlineColor(sf::Color(0x54, 0x38, 0x47));
	m_rectHsTable.setOutlineThickness(fs / 8);

	auto fsHs = hsH / 10;
	auto fColor = sf::Color::Black;

	auto fFont = [&](sf::Text& txt)
	{
		txt.setFont(m_font);
		txt.setCharacterSize(unsigned int(fsHs));

		txt.setFillColor(fColor);

		txt.setStyle(sf::Text::Bold);
	};

	{
		fFont(m_textHsLabelScore);
		auto bnds = m_textHsLabelScore.getLocalBounds();
		m_textHsLabelScore.setOrigin(bnds.width / 2, bnds.height / 2);
		m_textHsLabelScore.setPosition({ wsz.x / 2.f, wsz.y / 2.f - hsH / 2.f + fsHs * 3.f / 2.f });
	}

	{
		fFont(m_textHsLabelHighScore);
		auto bnds = m_textHsLabelHighScore.getLocalBounds();
		m_textHsLabelHighScore.setOrigin(bnds.width / 2, bnds.height / 2);
		m_textHsLabelHighScore.setPosition({ wsz.x / 2.f, wsz.y / 2.f - hsH / 2.f + fsHs * 5.5f });
	}

	{
		fColor = sf::Color::White;
		fsHs *= 2;
		fFont(m_textHsText);
		m_textHsText.setPosition({ wsz.x / 2.f, wsz.y / 2.f - hsH / 2.f + fsHs / 2.f * 5.5f });
		m_textHsText.setOutlineColor(sf::Color::Black);
		m_textHsText.setOutlineThickness(fs / 8);
	}

	restart();
}

bool Score::draw(const sf::Time& ts)
{
	switch (m_scoreState)
	{
	case EScoreSate::DRAW_SCORE:
		m_window.draw(m_text);
		break;
	case EScoreSate::DRAW_SCORE_TABEL:
	{
		m_window.draw(m_rectHsTable);
		m_window.draw(m_textHsLabelScore);
		m_window.draw(m_textHsLabelHighScore);

		auto pos = m_textHsText.getPosition();
		{
			auto m = std::to_wstring(m_scoreCarrent);
			m_textHsText.setString(m);
			auto bnds = m_textHsText.getLocalBounds();
			m_textHsText.setOrigin(bnds.width / 2, bnds.height / 2);
			m_textHsText.setPosition(pos.x, pos.y - bnds.height);
			m_window.draw(m_textHsText);
		}
		{
			auto m = std::to_wstring(m_scoreHigh);
			m_textHsText.setString(m);
			auto bnds = m_textHsText.getLocalBounds();
			m_textHsText.setOrigin(bnds.width / 2, bnds.height / 2);
			m_textHsText.setPosition(pos.x, pos.y + bnds.height);
			m_window.draw(m_textHsText);

		}
		m_textHsText.setPosition(pos);
	}

	break;
	default:
		break;
	}
	return true;
}

bool Score::restart()
{
	set(0);
	m_scoreState = EScoreSate::DRAW_SCORE;
	return true;
}

bool Score::stop()
{
	m_scoreState = EScoreSate::DRAW_SCORE_TABEL;
	return true;
}

void Score::set(int score)
{
	m_scoreCarrent = score;

	if (m_scoreHigh < m_scoreCarrent)
	{
		m_scoreHigh = m_scoreCarrent;
	}

	std::wstring ws = std::to_wstring(m_scoreCarrent) + L" : " + std::to_wstring(m_scoreHigh);

	m_text.setString(ws);
}

void Score::inc()
{
	auto s = m_scoreCarrent;
	set(++s);
}

void Score::dec()
{
	auto s = m_scoreCarrent;
	set(--s);
}
