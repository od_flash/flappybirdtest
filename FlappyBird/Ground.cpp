#include "Ground.h"
#include "ResourceManager.h"
#include "Settings.h"

#include <SFML/Graphics/RenderWindow.hpp>

#include <box2d/b2_world.h>
#include <box2d/b2_body.h>
#include <box2d/b2_math.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_fixture.h>

Ground::Ground(b2World& b2World, sf::RenderWindow& window, Settings& s)
	: IBase(b2World, window, s)
	, m_body(nullptr)
	, m_bodySky(nullptr)
	, m_fixture(nullptr)
	, m_b2StartPosition({})
{
	bool res = ResourceManager::loadGround(&m_textreGround);

	if (!res)
	{
		//@todo throw
		return;
	}

	auto szGround = m_textreGround.getSize();
	auto szWnd = m_window.getSize();
	szWnd.y += szGround.y;

	m_window.setSize(szWnd);

	{
		auto v = m_window.getView();
		sf::FloatRect fr(0.f, 0.f, float(szWnd.x), float(szWnd.y));
		v.reset(fr);
		m_window.setView(v);
	}

	m_textreGround.setRepeated(true);

	auto n = (int(szWnd.x) * 2) / szGround.x + 1;
	sf::IntRect rectTexture(0, 0, szGround.x * n, int(szGround.y));

	m_sprites.setTextureRect(rectTexture);
	m_sprites.setTexture(m_textreGround, false);

	m_sprites.setOrigin(float(rectTexture.width) / 2.f, float(rectTexture.height) / 2.f);
	
	res = createBodyGround();
	res &= createBodySky();

	restart();
}

bool Ground::draw(const sf::Time& ts)
{
	auto szWnd = m_window.getSize();
	auto szG = toWorld(szWnd);

	auto tr = m_body->GetTransform();

	if (tr.p.x < 0)
	{
		tr.p.x += m_BodySize.x / 2;
		m_body->SetTransform(tr.p, 0.f);
	}

	auto p = fromWorld(tr.p);

	m_sprites.setPosition({ float(p.x), float(p.y) });
	m_window.draw(m_sprites);

	return true;
}

bool Ground::restart()
{
	m_b2StartPosition = {};
	m_body->SetLinearVelocity({ -m_settings.GROUND_VELOCITY, 0.f });
	return true;
}

bool Ground::stop() 
{ 
	m_body->SetLinearVelocity({ 0.f, 0.f });
	return true;
}

EBodyType Ground::getBodyType(const b2Body* body) const
{
	return body == m_body ? EBodyType::GROUND : EBodyType::NONE;
}

inline bool Ground::createBodyGround()
{
	if (m_fixture != nullptr && m_body != nullptr)
	{
		m_body->DestroyFixture(m_fixture);
		m_fixture = nullptr;
	}

	if (m_body == nullptr)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2BodyType::b2_kinematicBody;
		bodyDef.gravityScale = 0;
		m_body = m_b2World.CreateBody(&bodyDef);
		m_body->SetUserData((void*)this);
	}

	b2PolygonShape box;
	{
		auto szS = m_sprites.getTextureRect();
		sf::Vector2u v(szS.width, szS.height);
		m_BodySize = toWorld(v);
		box.SetAsBox(m_BodySize.x / 2, m_BodySize.y / 2);
		box.m_radius = m_BodySize.x / 1000.0f;
	}

	b2FixtureDef fixtureDef;
	{
		fixtureDef.shape = &box;
		fixtureDef.density = 10.0f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0.5f;

		m_fixture = m_body->CreateFixture(&fixtureDef);
	}

	{
		auto sz = m_window.getSize();
		auto szGround = m_sprites.getTextureRect();
		sz.y -= szGround.height / 2;
		auto szG = toWorld(sz);

		m_body->SetTransform(szG, 0.f);
	}

	return true;
}

inline bool Ground::createBodySky()
{

	if (m_bodySky != nullptr)
	{
		m_b2World.DestroyBody(m_bodySky);
		m_bodySky = nullptr;
	}

	if (m_bodySky == nullptr)
	{
		b2BodyDef bodyDef;
		bodyDef.type = b2BodyType::b2_kinematicBody;
		bodyDef.gravityScale = 0;
		m_bodySky = m_b2World.CreateBody(&bodyDef);
		m_bodySky->SetUserData((void*)this);
	}

	auto szw = m_window.getView().getSize();
	auto szW = toWorld(szw);

	b2PolygonShape box;
	{
		box.SetAsBox(szW.x / 2, szW.y / 2);
		box.m_radius = szW.x / 300.0f;
	}

	b2FixtureDef fixtureDef;
	{
		fixtureDef.shape = &box;
		fixtureDef.density = 10.0f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0.5f;

		m_bodySky->CreateFixture(&fixtureDef);
	}

	auto pos = szW;

	pos.x = 0;
	pos.y = -szW.y / 2 ;

	m_bodySky->SetTransform(pos, 0.f);
	m_bodySky->SetLinearVelocity({ 0.f, 0.f });

	return true;
}
