#include "ResourceManager.h"

#include "resource.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>

#include <Windows.h>

std::vector<uint8_t> ResourceManager::s_fontBuffer;

bool ResourceManager::loadResourceFont(sf::Font* font, int idb, const wchar_t* s)
{
	if (s_fontBuffer.empty())
	{
		HRSRC hResource = FindResource(nullptr, MAKEINTRESOURCE(idb), s);

		if (hResource == nullptr)
		{
			return false;
		}

		HGLOBAL hMemory = LoadResource(nullptr, hResource);

		auto size = SizeofResource(nullptr, hResource);
		auto* ptr = LockResource(hMemory);

		s_fontBuffer.clear();
		s_fontBuffer.resize(size);

		std::memcpy(s_fontBuffer.data(), ptr, size);
	}

	if (!font->loadFromMemory(s_fontBuffer.data(), s_fontBuffer.size()))
	{
		return false;
	}

	return true;
}

bool ResourceManager::loadResource(sf::Texture* texture, int idb, const wchar_t* s)
{
	HRSRC hResource = FindResource(nullptr, MAKEINTRESOURCE(idb), s);

	if (hResource == nullptr)
	{
		return false;
	}

	HGLOBAL hMemory = LoadResource(nullptr, hResource);

	auto size = SizeofResource(nullptr, hResource);
	auto* ptr = LockResource(hMemory);

	std::vector<uint8_t> buf(size);
	std::memcpy(buf.data(), ptr, size);

	if (!texture->loadFromMemory(buf.data(), buf.size()))
	{
		return false;
	}

	return true;
}

bool ResourceManager::loadBackground(sf::Texture* texture)
{
	return loadResource(texture, IDB_PNG_BACKGROUND, L"PNG");
}

bool ResourceManager::loadGround(sf::Texture* texture)
{
	return loadResource(texture, IDB_PNG_GROUND, L"PNG");
}

bool ResourceManager::loadBird(sf::Texture* texture)
{
	return loadResource(texture, IDB_PNG_BIRD, L"PNG");
}

bool ResourceManager::loadPipe(sf::Texture* texture)
{
	return loadResource(texture, IDB_PNG_PIPE, L"PNG");
}

bool ResourceManager::loadFontScore(sf::Font* font)
{
	return loadResourceFont(font, IDR_FD2, L"FD");
}
