#pragma once

#include "IBase.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_math.h>

class b2World;
class b2Body;
class b2Fixture;
struct b2Vec2;

class Settings;

class MainMenu : protected IBase
{
public:
	MainMenu(b2World& b2World, sf::RenderWindow& window, Settings& s);

	bool draw(const sf::Time& ts) override final;
	bool restart() override final;
	bool stop() override final;

private:

	sf::Font m_font;
	sf::Text m_text;

	sf::RectangleShape m_rectHsTable;
	bool m_isDrawMainMenu;
};

