#pragma once

class Settings
{
public:
	int SCALE = 1000;
	int START_TIME_DIV = 1000;
	float GROUND_VELOCITY = 0.8f / 5.f;
	int WINGS_TIME_DIV = 50;
	int FONT_SIZE_PERCENT_WINDOW_HEIGHT = 5;
	int TABLE_HIGHSCORE_PERCENT_WINDOW_HEIGHT = 30;
	int CNT_PIPE_IN_WINDOW = 2;
	int PIPE_DISTANCE_IN_PERCENT_WINDOW_HEIGHT = 25;
	float impulse = -0.15f;
	float gravity = 1.f;
};

