#define _USE_MATH_DEFINES
#include <cmath>

#include "IBase.h"
#include "Bird.h"
#include "Settings.h"
#include "ResourceManager.h"


#include <SFML/Graphics/Texture.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_world.h>
#include <box2d/b2_body.h>
#include <box2d/b2_math.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_fixture.h>

#include <box2d/b2_contact.h>

Bird::Bird(b2World& b2World, sf::RenderWindow& window, Settings& s)
	: IBase(b2World, window, s)
	, m_lastTime(sf::Time::Zero)
	, m_body(nullptr)
	, m_fixture(nullptr)
	, m_isStarted(false)
	, m_isDied(false)
{
	bool res = ResourceManager::loadBird(&m_textreBird);

	if (!res)
	{
		//@todo throw
		return;
	}

	m_b2StartPosition.SetZero();

	init();
	restart();
}

bool Bird::draw(const sf::Time& ts)
{

	auto tMs = ts.asMilliseconds();

	if (!m_isStarted)
	{
		auto ttMs = tMs % m_settings.START_TIME_DIV;

		auto pos = m_b2StartPosition;
		pos.y *= 1.0f + float(sin(2.0f * M_PI * float(tMs) / float(m_settings.START_TIME_DIV))) / 10.0f;
		m_body->SetTransform(pos, 0);
	}
	else
	{
		int e = 1;
	}

	b2Vec2 position = m_body->GetPosition();

	{
		position.x = m_b2StartPosition.x;
		m_body->SetTransform(position, 0);
	}

	auto pw = fromWorld(position);

	auto sN = m_isDied ? 0 : (tMs / m_settings.WINGS_TIME_DIV) % m_sprites.size();
	m_sprites[sN].setPosition(float(pw.x), float(pw.y));
	m_window.draw(m_sprites[sN]);

	return true;
}

void Bird::jumpUp()
{
	if (m_body != nullptr)
	{
		if (!m_isStarted)
		{
			m_body->SetLinearVelocity({ 0.0f, 0.0f });
			m_isStarted = true;
		}
		m_body->ApplyLinearImpulseToCenter({ 0.0f, m_settings.impulse }, true);
	}
}

EContactResult Bird::contactResult(const b2Contact* contact)
{
	auto* fxtre = contact->GetFixtureA();

	const auto* body = fxtre->GetBody();

	if (body->GetUserData() == this)
	{
		fxtre = contact->GetFixtureB();
		body = fxtre->GetBody();
	}

	auto* base = static_cast<IBase*>(body->GetUserData());
	switch (base->getBodyType(body))
	{

	case EBodyType::PIPE:
		return EContactResult::LOSE;

	case EBodyType::GROUND:
		return EContactResult::LOSE;

	case EBodyType::SENSOR:
		return EContactResult::SCORE;

	default:
		break;
	}

	return EContactResult::NONE;
}

inline bool Bird::restart()
{
	bool res = recreateBodyBird();
	m_isStarted = false;
	m_isDied = false;
	return res;
}

bool Bird::stop()
{
	m_isDied = true;
	return true;
}

inline bool Bird::init()
{

	if (m_sprites.empty())
	{
		auto sz = m_textreBird.getSize();
		int wx = int(sz.x / 3);
		sf::IntRect rect = { 0, 0, wx, int(sz.y) };

		for (auto i : { 0, 1, 2 })
		{
			m_sprites.emplace_back(m_textreBird, rect);
			m_sprites.back().setOrigin(wx / 2.f, sz.y / 2.f);
			rect.left += wx;
		}
	}

	return true;
}

bool Bird::recreateBodyBird()
{
	if (m_fixture != nullptr)
	{
		if (m_body != nullptr)
		{
			m_body->DestroyFixture(m_fixture);
			m_fixture = nullptr;

			m_b2World.DestroyBody(m_body);
			m_body = nullptr;
		}
	}

	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_dynamicBody;
	bodyDef.bullet = true;

	m_body = m_b2World.CreateBody(&bodyDef);
	m_body->SetUserData((void*)this);

	auto szBird = m_textreBird.getSize();
	szBird.x /= 3;

	auto szWb = toWorld(szBird);
	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox(szWb.x / 2, szWb.y / 2);
	dynamicBox.m_radius = szWb.x / 300.0f;

	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 50.0f;
	fixtureDef.friction = 0.f;
	fixtureDef.restitution = 0.5f;
	m_fixture = m_body->CreateFixture(&fixtureDef);

	{
		const sf::Vector2u sz = m_window.getSize();

		m_b2StartPosition = toWorld(sz);
		m_b2StartPosition.x /= 3.f;
		m_b2StartPosition.y /= 2.5f;
	}

	m_body->SetTransform(m_b2StartPosition, 0);
	m_body->SetLinearVelocity({ 0.f, 0.f });
	return true;
}