#pragma once
#include "IBase.h"

#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Vector2.hpp>

#include <box2d/b2_math.h>

namespace sf
{
	class Texture;
	class Time;
}

class b2Fixture;
class b2Body;

class Ground : protected IBase
{
public:
	Ground(b2World& b2World, sf::RenderWindow& window, Settings& s);;
	bool draw(const sf::Time& ts) override final;
	bool restart() override final;
	bool stop() override final;
	EBodyType getBodyType(const b2Body* body) const final override;

private:
	bool createBodyGround();
	bool createBodySky();
	sf::Texture m_textreGround;
	b2Fixture* m_fixture;
	
	b2Body* m_body;
	b2Body* m_bodySky;

	b2Vec2 m_BodySize;

    sf::Sprite m_sprites;

	b2Vec2 m_b2StartPosition;

};

