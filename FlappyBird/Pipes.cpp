#include "Pipes.h"

#include "ResourceManager.h"
#include "Settings.h"

#include <SFML/Graphics/RenderWindow.hpp>

#include <box2d/b2_world.h>
#include <box2d/b2_body.h>
#include <box2d/b2_math.h>
#include <box2d/b2_polygon_shape.h>
#include <box2d/b2_fixture.h>

Pipes::Pipes(b2World& b2World, sf::RenderWindow& window, Settings& s)
	: IBase(b2World, window, s)
	, m_isStarted(false)
{
	bool res = ResourceManager::loadPipe(&m_textrePipe);

	if (!res)
	{
		//@todo throw
		return;
	}

	auto sz = m_textrePipe.getSize();
	spriteDown.setTexture(m_textrePipe);
	spriteDown.setOrigin(sz.x / 2.f, sz.y / 2.f);

	spriteUp.setRotation(180.0f);
	spriteUp.setOrigin(sz.x / 2.f, sz.y / 2.f);

	spriteUp.setTexture(m_textrePipe);
	restart();

}

bool Pipes::draw(const sf::Time& ts)
{
	const auto wsz = m_window.getView().getSize();
	const auto tsz = m_textrePipe.getSize();

	const auto f = [&](b2Body* body, sf::Sprite& spite, bool isPipeOut, float shiftY, bool isDraw = true)
	{
		b2Vec2 pos = body->GetPosition();
		auto p = fromWorld(pos);

		if (isPipeOut)
		{
			pos.y += shiftY;

			const auto szW = toWorld(wsz);
			auto stepX = szW.x / m_settings.CNT_PIPE_IN_WINDOW;
			pos.x += stepX * (m_settings.CNT_PIPE_IN_WINDOW + 1);
			body->SetTransform(pos, 0.f);
			p = fromWorld(pos);
		}

		spite.setPosition(float(p.x), float(p.y));
		if (isDraw)
		{
			m_window.draw(spite);
		}
	};

	for (auto& i : m_pipes)
	{
		b2Vec2 pos = i.pipeUp->GetPosition();
		auto p = fromWorld(pos);

		bool isPipeOut = p.x < -float(tsz.x) / 2.f;

		float shiftY = -i.shiftY;

		if (isPipeOut)
		{
			const auto szW = toWorld(wsz);
			const auto v = szW.y * float(m_settings.PIPE_DISTANCE_IN_PERCENT_WINDOW_HEIGHT) / 100.f / 2;
			std::uniform_real_distribution<float> uniform_dist(-v / 1.3f, v / 1.3f);
			shiftY += uniform_dist(e1);
		}

		f(i.pipeUp, spriteUp, isPipeOut, shiftY);
		f(i.pipeDown, spriteDown, isPipeOut, shiftY);
		f(i.pipeSensor, spriteDown, isPipeOut, shiftY, false);
		i.shiftY += shiftY;
	}

	return true;
}

bool Pipes::restart()
{
	m_isStarted = false;
	recreatePipesVector();
	return true;
}

bool Pipes::stop() 
{
	for (const auto& i : m_pipes)
	{
		i.pipeUp->SetLinearVelocity({ 0.f, 0.f });
		i.pipeDown->SetLinearVelocity({ 0.f, 0.f });
		i.pipeSensor->SetLinearVelocity({ 0.f, 0.f });
	}
	return true;
}

EBodyType Pipes::getBodyType(const b2Body* body) const
{
	for (const auto& i : m_pipes)
	{
		if (body == i.pipeDown || body == i.pipeUp)
		{
			return EBodyType::PIPE;
		}

		if (body == i.pipeSensor)
		{
			return EBodyType::SENSOR;
		}
	}

	return EBodyType::NONE;
}

void Pipes::jumpUp()
{
	if (!m_isStarted)
	{
		for (auto& i : m_pipes)
		{
			i.pipeUp->SetLinearVelocity({ -m_settings.GROUND_VELOCITY, 0.f });
			i.pipeDown->SetLinearVelocity({ -m_settings.GROUND_VELOCITY, 0.f });
			i.pipeSensor->SetLinearVelocity({ -m_settings.GROUND_VELOCITY, 0.f });
		}
		m_isStarted = true;
	}
}

inline void Pipes::recreatePipesVector()
{
	for (auto& i : m_pipes)
	{
		if (i.pipeUp != nullptr)
		{
			m_b2World.DestroyBody(i.pipeUp);
		}

		if (i.pipeDown != nullptr)
		{
			m_b2World.DestroyBody(i.pipeDown);
		}

		if (i.pipeSensor != nullptr)
		{
			m_b2World.DestroyBody(i.pipeSensor);
		}

	}

	m_pipes.clear();

	const auto wsz = m_window.getView().getSize();
	const auto tsz = m_textrePipe.getSize();
	const auto szW = toWorld(wsz);
	const auto szT = toWorld(tsz);

	auto stepX = szW.x / m_settings.CNT_PIPE_IN_WINDOW;

	b2Vec2 posUp, posDown, posSensor;
	posUp.x = posDown.x = posSensor.x = szW.x + szT.x / 2.f;

	int cnt = m_settings.CNT_PIPE_IN_WINDOW + 1;

	const auto v = szW.y * float(m_settings.PIPE_DISTANCE_IN_PERCENT_WINDOW_HEIGHT) / 100.f / 2;
	std::uniform_real_distribution<float> uniform_dist(-v / 1.3f, v / 1.3f);

	while (cnt--)
	{
		float shiftY = uniform_dist(e1);

		posUp.y = szW.y / 2.f - v + shiftY;
		posUp.y -= szT.y / 2.f;
		posDown.y = szW.y / 2.f + v + shiftY;
		posDown.y += szT.y / 2.f;
		posSensor.y = szW.y / 2.f + shiftY;

		auto* bodyUp = createPipeBody(posUp);
		auto* bodyDown = createPipeBody(posDown);
		auto* bodySensor = createPipeBodySensor(posSensor);

		posUp.x += stepX;
		posDown.x += stepX;
		posSensor.x += stepX;

		m_pipes.emplace_back(bodyUp, bodyDown, bodySensor, shiftY);
	}
}

b2Body* Pipes::createPipeBody(const b2Vec2& pos) const
{
	b2Body* body = nullptr;

	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_kinematicBody;
	bodyDef.gravityScale = 0;

	body = m_b2World.CreateBody(&bodyDef);
	body->SetUserData((void*)this);

	auto szPipe = m_textrePipe.getSize();

	auto szP = toWorld(szPipe);

	b2PolygonShape box;
	{
		box.SetAsBox(szP.x / 2, szP.y / 2);
		box.m_radius = szP.x / 100.0f;
	}

	b2FixtureDef fixtureDef;
	{
		fixtureDef.shape = &box;
		fixtureDef.density = 10.0f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0.5f;

		auto* fxt = body->CreateFixture(&fixtureDef);
	}

	body->SetTransform(pos, 0);
	return body;
}

b2Body* Pipes::createPipeBodySensor(const b2Vec2& pos) const
{
	b2Body* body = nullptr;

	b2BodyDef bodyDef;
	bodyDef.type = b2BodyType::b2_kinematicBody;
	bodyDef.gravityScale = 0;

	body = m_b2World.CreateBody(&bodyDef);
	body->SetUserData((void*)this);

	auto szPipe = m_textrePipe.getSize();

	auto szP = toWorld(szPipe);

	b2PolygonShape box;
	{
		box.SetAsBox(szP.x / 20000.f, szP.y / 2.f);
		box.m_radius = szP.x / 20000.f;
	}

	b2FixtureDef fixtureDef;
	{
		fixtureDef.shape = &box;
		fixtureDef.density = 10.0f;
		fixtureDef.friction = 0.0f;
		fixtureDef.restitution = 0.5f;
		fixtureDef.isSensor = true;

		auto* fxt = body->CreateFixture(&fixtureDef);
	}

	body->SetTransform(pos, 0);
	return body;
}
